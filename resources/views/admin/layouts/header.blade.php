<nav class="navbar navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand">Admin Dashboard</a>
            <a class="nav-link" href="{{ route('jobs') }}">{{ __('Jobs') }}</a>
            <a class="nav-link" href="{{ route('applied-job-list') }}">{{ __('Applied Job') }}</a>
      <a href="/admin/logout">Logout</a>
    </div>
</nav>