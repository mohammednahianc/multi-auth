@include('admin.layouts.header_link')

@include('admin.layouts.header')
<body>
	@yield('content')
</body>

@include('admin.layouts.footer_link')