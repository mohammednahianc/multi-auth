@extends('layouts.app')

@section('content')

    <table class="table">
        <thead>
        <th>Applied By</th>
        </thead>
        <tbody>
        @foreach($applied_job_list as $key => $job)
            <tr>
                <td>{{ !empty($job->user_id)?$job->user_id:'' }}</td>
                </tr>
        @endforeach
        </tbody>
    </table>

@endsection

