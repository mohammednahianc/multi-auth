@extends('layouts.app')

@section('content')
@if(auth()->user()->user_type==1)
<div class="card-tools">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add_modal" ><i class="fas fa-plus-circle"></i> Add New Job</button>
</div>
@endif
<table class="table">
              <thead>
                <th>Employer</th>
                <th>Job Title</th>
                <th>Job Type</th>
                <th>Job Description</th>
                <th>Job Closing Date</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($job_list as $key => $job)
                <tr>
                  <td>{{$job->user->name}}</td>
                  <td>{{$job->job_title}}</td>
                  <td>{{$job->job_type}}</td>
                  <td>{{$job->job_description}}</td>
                  <td>{{$job->job_closing_date}}</td>
                  <td>

                    @if(auth()->user()->user_type==2)
                    <a style="border: 2px solid #00539f;color:#00539f;padding: 10px 25px 10px 25px;border-radius: 25px;margin-right: 2%" data-id="{{$job->id}}" class="apply_job float-left" >Apply</a>
                    <a style="border: 2px solid #00539f;color:#00539f;padding: 10px 25px 10px 25px;border-radius: 25px;margin-right: 2%" href="{{url('job_details')}}/{{ $job->id }}" class="float-left" >View</a>
                    @else
                          <a style="border: 2px solid #00539f;color:#00539f;padding: 10px 25px 10px 25px;border-radius: 25px;margin-right: 2%" href="{{url('job_details')}}/{{ $job->id }}" class="float-left" >View</a>
                          <form method="post" action="{{url('delete_job/'.$job->id)}}">
                              @csrf
                              @method('DELETE')
                              <button style="border: 2px solid #00539f;color:#fff;background-color: #00539f; padding: 10px 25px 10px 25px;border-radius: 25px" type="submit" class="btn btn-danger btn-sm">Delete</button>
                          </form>

                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>

            <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="add_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_modal_label">Add Job</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_form">

                    <div class="modal-body">

                        <h4 id="success_msg" style="color: green;font-weight: 600;"></h4>
                        <div class="form-group">
                            <label >Job Title</label>
                            <input type="text" class="form-control" name="job_title" id="job_title" placeholder="Enter job Title">
                        </div>
                        <div class="form-group">
                            <label >Job Description</label>
                            <textarea id="job_description" type="text" class="form-control" name="job_description"> </textarea>
                        </div>
                        <div class="form-group">
                            <label >Job Type</label>
                            <input type="text" class="form-control" name="job_type" id="job_type" placeholder="Enter job Type">
                        </div>
                        <div class="form-group">
                            <label >Job Closing Date</label>
                            <input type="date" class="form-control" name="job_closing_date" id="job_closing_date">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="add_btn" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

        <div class="modal fade" id="add_modal2" tabindex="-1" role="dialog" aria-labelledby="add_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_modal_label">Apply This Job</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_job_form">

                    <div class="modal-body">

                        <input type="hidden" name="job_id" id="job_id">

                        <h4 id="success_msg" style="color: green;font-weight: 600;"></h4>

                        <div class="form-group">
                            <label >Fathers Name</label>
                            <input type="text" class="form-control" name="fathers_name" id="fathers_name" placeholder="Enter Fathers Name">
                        </div>
                        <div class="form-group">
                            <label >Mothers Name</label>
                            <input type="text" class="form-control" name="mothers_name" id="mothers_name" placeholder="Enter Mothers Name">
                        </div>
                        <div class="form-group">
                            <label >Present Address</label>
                            <textarea type="text" class="form-control summernote" name="present_address"  placeholder="Enter Present Address"> </textarea>
                        </div>
                        <div class="form-group">
                            <label >Permanent Address</label>
                            <textarea type="text" class="form-control summernote" name="permanent_address"  placeholder="Enter Permanent Address"> </textarea>
                        </div>
                        <div class="form-group">
                            <label >Date Of Birth</label>
                            <input type="date" class="form-control" name="date_of_birth" id="date_of_birth" placeholder="Enter Date Of Birth">
                        </div>
                        <div class="form-group">
                            <label >Expected Salary</label>
                            <input type="text" class="form-control" name="expected_salary" id="expected_salary" placeholder="Enter Expected Salary">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="add_apply_btn" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
        </div>


<script type='text/javascript'>
    $("#add_btn").click(function (){
        $(".error_msg").html('');
        var data = new FormData($('#add_form')[0]);
        $.ajax({
             headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
            method: "POST",
            url: "jobs",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data, textStatus, jqXHR) {

            }
        }).done(function() {
            $("#success_msg").html("Data Save Successfully");
            location.reload();
        }).fail(function(data, textStatus, jqXHR) {
            var json_data = JSON.parse(data.responseText);
            $.each(json_data.errors, function(key, value){
                $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
            });
        });
    });

    $(".apply_job").click(function (){
        var id = $(this).attr('data-id');
        $('#job_id').val(id);
        $('#add_modal2').modal('show');
    });

    $("#add_apply_btn").click(function (){
        $(".error_msg").html('');
        var data = new FormData($('#add_job_form')[0]);
        $.ajax({
             headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
            method: "POST",
            url: "applied_jobs",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data, textStatus, jqXHR) {

            }
        }).done(function() {
            $("#success_msg").html("Data Save Successfully");
            location.reload();
        }).fail(function(data, textStatus, jqXHR) {
            var json_data = JSON.parse(data.responseText);
            $.each(json_data.errors, function(key, value){
                $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
            });
        });
    });


</script>



@endsection

