@extends('layouts.app')

@section('content')
    @if(auth()->user()->user_type==1)
        <div class="card-tools">
            <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#add_modal" ><i class="fas fa-plus-circle"></i> Add New Job</button>
        </div>
    @endif
    @method('PUT')
    <input type="hidden" name="id" value="{{ $job_list->id }}">

    <table class="table">
        <tr>
        <td>Employer</td>
            <td>{{$job_list->user->name}}</td>
        </tr>
        <tr>
            <td>Job Title</td>
            <td>{{$job_list->job_title}}</td>
        </tr>
        <tr>
            <td>Job Description</td>
            <td>{{$job_list->job_description}}</td>
        </tr>
        <tr>
            <td>Job Type</td>
            <td>{{$job_list->job_type}}</td>
        </tr>
        <tr>
            <td>Job Closing Date</td>
            <td>{{$job_list->job_closing_date}}</td>
        </tr>
        <tbody>

            <tr>

            </tr>
        </tbody>
    </table>

    <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="add_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_modal_label">Add Job</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_form">

                    <div class="modal-body">

                        <h4 id="success_msg" style="color: green;font-weight: 600;"></h4>
                        <div class="form-group">
                            <label >Job Title</label>
                            <input type="text" class="form-control" name="expected" id="job_title" placeholder="Enter job Title">
                        </div>
                        <div class="form-group">
                            <label >Job Description</label>
                            <textarea id="job_description" type="text" class="form-control" name="job_description"> </textarea>
                        </div>
                        <div class="form-group">
                            <label >Job Type</label>
                            <input type="text" class="form-control" name="job_type" id="job_type" placeholder="Enter job Type">
                        </div>
                        <div class="form-group">
                            <label >Job Closing Date</label>
                            <input type="date" class="form-control" name="job_closing_date" id="job_closing_date">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="add_btn" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_modal2" tabindex="-1" role="dialog" aria-labelledby="add_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_modal_label">Apply This Job</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_job_form">

                    <div class="modal-body">

                        <input type="number" name="applied_job_id" id="applied_job_id">

                        <h4 id="success_msg" style="color: green;font-weight: 600;"></h4>
                        <div class="form-group">
                            <label >Expected Salay</label>
                            <input type="text" class="form-control" name="expected_salary" id="expected_salary" placeholder="Enter Expected Salary">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="add_apply_btn" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_modal3" tabindex="-1" role="dialog" aria-labelledby="add_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add_modal_label">View Job</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_job_form">

                    <div class="modal-body">

                        <input type="number" name="job_id" id="job_id">

                        <h4 id="success_msg" style="color: green;font-weight: 600;"></h4>
                        <div class="form-group">
                            <label >Job Title</label>
                            <input type="text" class="form-control" name="job_title" id="job_title">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="add_apply_btn" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type='text/javascript'>
        $("#add_btn").click(function (){
            $(".error_msg").html('');
            var data = new FormData($('#add_form')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "jobs",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {

                }
            }).done(function() {
                $("#success_msg").html("Data Save Successfully");
                location.reload();
            }).fail(function(data, textStatus, jqXHR) {
                var json_data = JSON.parse(data.responseText);
                $.each(json_data.errors, function(key, value){
                    $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
                });
            });
        });

        $(".apply_job").click(function (){
            var id = $(this).attr('data-id');
            $('#applied_job_id').val(id);
            $('#add_modal2').modal('show');
        });

        $("#add_apply_btn").click(function (){
            $(".error_msg").html('');
            var data = new FormData($('#add_job_form')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "applied_jobs",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {

                }
            }).done(function() {
                $("#success_msg").html("Data Save Successfully");
                location.reload();
            }).fail(function(data, textStatus, jqXHR) {
                var json_data = JSON.parse(data.responseText);
                $.each(json_data.errors, function(key, value){
                    $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
                });
            });
        });


    </script>
    <script type='text/javascript'>
        $("#add_btn").click(function (){
            $(".error_msg").html('');
            var data = new FormData($('#add_form')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "jobs",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {

                }
            }).done(function() {
                $("#success_msg").html("Data Save Successfully");
                location.reload();
            }).fail(function(data, textStatus, jqXHR) {
                var json_data = JSON.parse(data.responseText);
                $.each(json_data.errors, function(key, value){
                    $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
                });
            });
        });

        $(".view_job").click(function (){
            var id = $(this).attr('data-id');
            $('#job_id').val(id);
            $('#add_modal3').modal('show');
            $('#job_title').html('job_title');
            $('#add_modal3').modal('show');
        });

        $("#add_apply_btn").click(function (){
            $(".error_msg").html('');
            var data = new FormData($('#add_job_form')[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: "applied_jobs",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {

                }
            }).done(function() {
                $("#success_msg").html("Data Save Successfully");
                location.reload();
            }).fail(function(data, textStatus, jqXHR) {
                var json_data = JSON.parse(data.responseText);
                $.each(json_data.errors, function(key, value){
                    $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
                });
            });
        });


    </script>



@endsection

