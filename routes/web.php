<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->group(function() {
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'Auth\AdminController@index')->name('admin.dashboard');

    Route::get('/jobs', [App\Http\Controllers\AdminJobController::class, 'index'])->name('jobs');
    Route::get('/applied_job_list', [App\Http\Controllers\AppliedJobController::class, 'index'])->name('applied-job-list');
    Route::get('/job_details/{any}', [App\Http\Controllers\JobController::class, 'show'])->name('job-details');
}) ;


Route::get('/jobs', [App\Http\Controllers\JobController::class, 'index'])->name('jobs');
Route::post('/jobs', [App\Http\Controllers\JobController::class, 'store'])->name('jobs');
Route::get('/applied_job_list', [App\Http\Controllers\AppliedJobController::class, 'index'])->name('applied-job-list');
Route::get('/job_details/{any}', [App\Http\Controllers\JobController::class, 'show'])->name('job-details');
Route::resource('delete_job', 'JobController');
Route::post('/applied_jobs', [App\Http\Controllers\AppliedJobController::class, 'store'])->name('applied-jobs');
Route::get('/generate-password',function(Request $request){
    if($request->p) {
        return bcrypt($request->p);
    }
    return 'Password plain text required!';
});
