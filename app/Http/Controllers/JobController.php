<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_type=auth()->user()->user_type;
        $user_id=auth()->user()->id;
        /*if($user_type == 2){
            $job_list=Job::where('user_id',$user_id)->get();
        }else{
            $job_list=Job::all();
        }*/
        $job_list=Job::all();

        return view('job.job_list',compact('job_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd('request->all()');

         $request->validate([
            'job_title' => 'required|max:255',

        ]);

        $job = new Job;
        $job->user_id = auth()->user()->id;
        $job->job_title = $request->job_title;
        $job->job_description = $request->job_description;
        $job->job_type = $request->job_type;
        $job->job_closing_date = date( "Y-m-d", strtotime($request->job_closing_date));
        $job->save();
    }

    /**
     * Display the specifie d resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job_list= Job::findOrFail($id); //This will fetch the respective record from the table.
        return view('job.job_details',compact('job_list'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::find($id);

        $job->delete();
        return redirect('jobs');
    }
}
