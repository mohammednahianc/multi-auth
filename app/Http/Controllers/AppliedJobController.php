<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AppliedJob;

class AppliedJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_type=auth()->user()->user_type;
        $user_id=auth()->user()->id;
        if($user_type==2)
        {
            $applied_job_list=AppliedJob::where('user_id',$user_id)->get();
        }else{
            $applied_job_list=AppliedJob::all();
        }

        return view('applied_job.applied_job_list',compact('applied_job_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'expected_salary' => 'required|max:255',

        ]);

        $applied_job = new AppliedJob;
        $applied_job->fathers_name = $request->fathers_name;
        $applied_job->mothers_name = $request->mothers_name;
        $applied_job->present_address = $request->present_address;
        $applied_job->permanent_address = $request->permanent_address;
        $applied_job->date_of_birth = $request->date_of_birth;
        $applied_job->expected_salary = $request->expected_salary;
        $applied_job->job_id = $request->job_id;
        $applied_job->user_id = auth()->user()->id;

        $applied_job->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
