<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppliedJob extends Model
{
    use HasFactory;
    protected $table = 'applied_jobs';

    public function job()
    {
        return $this->belongsTo(Job::class,'job_id','id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }



}
